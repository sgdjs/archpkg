# archpkg

This is my meta-package to quickly install the packages I need on my
system.

## Dependencies

* Arch base installation
* Git
* base-devel

## Build

``` shell
makepkg
```
